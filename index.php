<?php
// This will load the framework
require 'vendor/autoload.php';
use Psr\Http\Message\ServerRequestInterface;

$app = new \Slim\App;

// To prevent problems with CORS
$app->add(new Tuupola\Middleware\CorsMiddleware);


// List all products
$app->get('/products', function(ServerRequestInterface $request){
	// Get the page that we want to display, if no value given we start in page 0
	$page = $request->getQueryParam('page', $default=0);
	// Get the number of elements that we want to load, by default 10
	$limit = $request->getQueryParam('limit', $default=10);
	$offset = $page * $limit;
	// Load the content of the JSON file
    $str = file_get_contents(__DIR__ . '/products.json');
    $products = json_decode($str, true);
    // We take the number of elements starting in the position specify
    $set = array_slice($products['products'], $offset, $limit);
    return json_encode(array('products' => $set, 'total' => count($products['products'])));
});

$app->run();